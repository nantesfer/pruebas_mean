var express = require('express');
var app = express();
var path = require('path');
var bodyParser = require('body-parser');

var port = 8080;

var index = require('./routes/index');
var tasks = require('./routes/tasks');

//Establecemos el motor de vistas.
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

//Middleware para servir archivos estáticos.
app.use(express.static(path.join(__dirname,'client')));

//Middleware de body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

//Ruta para la homepage y las tareas.
app.use('/', index);
app.use('/api', tasks);

app.listen(port, function(){
    console.log('Servidor escuchando en '+port);
})